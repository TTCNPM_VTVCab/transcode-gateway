import React, { useState, useEffect, useRef } from "react"
import { Table, Pagination } from "components/ui"
import columns from "./Columns"
import { paramsUrl } from "lib/function"
import { apiTranscode } from "api"

const Component = () => {
  const [rows, setRows] = useState([])
  const [loading, setLoading] = useState(false)

  const __pagination = useRef({
    page_num: 1,
    page_size: 100,
    count: 0,
    ...paramsUrl.get()
  })

  async function fetch() {
    let rows = []
    try {
      setLoading(true)
      const { results, count } = await apiTranscode.getListTranscode(__pagination.current)

      rows = results
      __pagination.current.count = count
    } catch (e) {
      throw e
    } finally {
      setRows(rows)
      setLoading(false)
    }
    paramsUrl.set(__pagination.current)
  }

  function changePage(page_num, page_size) {
    __pagination.current.page_num = page_num
    __pagination.current.page_size = page_size
    fetch()
  }

  useEffect(() => {
    fetch()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  return (
    <section>
      <div className="__content">
        <Table
          size="small"
          pagination={false}
          dataSource={rows}
          loading={loading}
          columns={columns()}
        />
        <Pagination {...__pagination.current} onChange={changePage} />
      </div>
    </section>
  )
}

export default Component
