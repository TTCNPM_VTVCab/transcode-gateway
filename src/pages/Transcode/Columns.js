import { Progress } from "antd"

const twoColors = { "0%": "#108ee9", "100%": "#87d068" }

export default function columns(onAction) {
  return [
    {
      title: "ID",
      dataIndex: "id",
      key: "id",
      width: 40
    },
    {
      title: "Config ID",
      dataIndex: "configId",
      key: "configId",
      width: 70
    },
    {
      title: "Input File",
      dataIndex: "inputFile",
      key: "inputFile",
      width: 250
    },
    {
      title: "Priority",
      dataIndex: "priority",
      key: "priority",
      width: 30
    },
    {
      title: "Infra",
      dataIndex: "infra",
      key: "infra",
      width: 30
    },
    {
      title: "Infra JobId",
      dataIndex: "infraJobId",
      key: "infraJobId",
      width: 110
    },
    {
      title: "State",
      dataIndex: "state",
      key: "state",
      width: 40,
      filters: [
        {
          text: "Encoding",
          value: "encoding"
        },
        {
          text: "Done",
          value: "done"
        }
      ],
      onFilter: (value, record) => record.state.indexOf(value) === 0
    },
    {
      title: "Completion",
      dataIndex: "completion",
      key: "completion",
      width: 60,
      sorter: (a, b) => a.completion - b.completion,
      render: (completion) => (
        <div style={{ width: 100 }}>
          <Progress percent={completion} strokeColor={twoColors} />
        </div>
      )
    }
  ]
}
