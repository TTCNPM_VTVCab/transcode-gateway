const INPUT = {
  bang_chung: "2ea3b060-90d9-48cd-8ec8-7869a13b7121",
  BANG_CHO: "f33b7a71-4648-4edd-a919-2504eb7009f2",
  DOI_HINH: "d3f5b744-305d-436d-93ac-023331f53039",
  TRONG_TAI: "cccf7a7d-e8e0-4378-87df-4136cc46329d",
  HLV_CN: "21b8af47-2a53-46a1-b347-d1c03790fda2",
  HLV_KHACH: "5010e3df-9da4-4986-8b23-86bb63b1cfda",
  BAR_TS: "0641f117-b794-4705-8d17-14b001665720",
  TI_SO_TO: "2602cb24-b179-40b4-9eb0-1ca59fac5aaa",
  TI_SO_TO_FULL: "8f7e18b2-27e4-49c8-b62d-73b8ddc962b7",
  BXH: "917230a0-c6e1-4336-9a11-e4a6c5b1b437",
  TEN_PHAT_BIEU: "11d545eb-66ee-4929-bfa2-388e34957e8f",
  BAR_TEN: "53d4fb6b-e55e-4db0-a43c-d1a585f374dc",
  BAR_TEN_DAI: "343a1553-f0c5-4819-a50f-43227084b7fa",
  THONG_SO_NHO: "981c1632-551a-4e8d-b118-22264d749971",
  TEN_PHAT_BIEU_1: "91f8a6bf-0038-44f3-af6e-ecbc9bd5438b"
}

export default INPUT

export const listQuarter = [
  {
    id: 1,
    value: "First quarter",
    name: "First quarter",
    thumbBig: "D:/VBA 2023/ROUND TITLE/E_1.png",
    thumbSmall: "D:/VBA 2023/ROUND TITLE/1st.png"
  },
  {
    id: 2,
    value: "Second quarter",
    name: "Second quarter",
    thumbBig: "D:/VBA 2023/ROUND TITLE/E_2.png",
    thumbSmall: "D:/VBA 2023/ROUND TITLE/2nd.png"
  },
  {
    id: 3,
    value: "Third quarter",
    name: "Third quarter",
    thumbBig: "D:/VBA 2023/ROUND TITLE/E_3.png",
    thumbSmall: "D:/VBA 2023/ROUND TITLE/3rd.png"
  },
  {
    id: 4,
    value: "Fourth quarter",
    name: "Fourth quarter",
    thumbBig: "D:/VBA 2023/ROUND TITLE/E_4.png",
    thumbSmall: "D:/VBA 2023/ROUND TITLE/4th.png"
  }
  // { value: "Overtime 1", name: "Overtime 1", thumbBig: "", thumbSmall: "" },
  // { value: "Overtime 2", name: "Overtime 2", thumbBig: "", thumbSmall: "" }
]

export const listFoul = [
  { value: 0, name: "0" },
  { value: 1, name: "1" },
  { value: 2, name: "2" },
  { value: 3, name: "3" },
  { value: 4, name: "4" },
  { value: 5, name: "5" }
]

export const listStatF = [
  { value: "PTS", name: "PTS" },
  { value: "REBS", name: "REBS" },
  { value: "ASTS", name: "ASTS" },
  { value: "BLKS", name: "BLKS" },
  { value: "STLS", name: "STLS" },
  { value: "FG(%)", name: "FG(%)" },
  { value: "3P(%)", name: "3P(%)" },
  { value: "FT", name: "FT" },
  { value: "PPG", name: "PPG" },
  { value: "RPG", name: "RPG" },
  { value: "APG", name: "APG" },
  { value: "POS", name: "POS" },
  { value: "HEIGHT", name: "HEIGHT" },
  { value: "WEIGHT", name: "WEIGHT" },
  { value: "TO", name: "TO" },
  { value: "MINS", name: "MINS" },
  { value: "MPG", name: "MPG" }
]

export const listPosition = [
  { value: "|FORWARD", name: "FORWARD" },
  { value: "|GUARD", name: "GUARD" },
  { value: "|CENTER", name: "CENTER" },
  { value: "|GAME", name: "GAME" }
]

export const listCasters = [
  { value: "BÙI ĐỨC HUY", name: "BÙI ĐỨC HUY" },
  { value: "VŨ PHƯƠNG ĐẠO", name: "VŨ PHƯƠNG ĐẠO" },
  { value: "NGUYỄN MINH HIẾU", name: "NGUYỄN MINH HIẾU" },
  { value: "TRƯƠNG HOÀNG TRUNG", name: "TRƯƠNG HOÀNG TRUNG" },
  { value: "NGUYỄN QUỐC VIỆT", name: "NGUYỄN QUỐC VIỆT" },
  { value: "TRẦN TRUNG HIẾU", name: "TRẦN TRUNG HIẾU" }
]

export const listReferees = [
  { value: "DOMINIQUE POMAR", name: "DOMINIQUE POMAR" },
  { value: "HARRY SANTOS", name: "HARRY SANTOS" },
  { value: "THÁI VIỆT HƯNG", name: "THÁI VIỆT HƯNG" },
  { value: "LÊ TRƯỜNG GIANG", name: "LÊ TRƯỜNG GIANG" },
  { value: "AARON CANETE", name: "AARON CANETE" },
  { value: "NGUYỄN SƠN TÙNG", name: "NGUYỄN SƠN TÙNG" },
  { value: "LÊ PHẠM TRUNG KIÊN", name: "LÊ PHẠM TRUNG KIÊN" }
]

export const teamColor = [
  {
    name: "DA CAM",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/DA CAM.png"
  },
  {
    name: "ĐEN",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/DEN.png"
  },
  {
    name: "ĐỎ THẪM",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/DO THAM.png"
  },
  {
    name: "ĐỎ",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/DO.png"
  },
  {
    name: "HỒNG",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/HONG.png"
  },
  {
    name: "TÍM ĐẬM",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/TIM DAM.png"
  },
  {
    name: "TÍM NHẠT",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/TIM NHAT.png"
  },
  {
    name: "TÍM PHA KE",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/TIM PHA KE.png"
  },
  {
    name: "TRẮNG",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/TRANG.png"
  },
  {
    name: "VÀNG NHẠT",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/vang nhat.png"
  },
  {
    name: "VÀNG",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/VANG.png"
  },
  {
    name: "XANH BLUE",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/XANH BLUE.png"
  },
  {
    name: "XANH BỘ ĐỘI",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/XANH BO DOI.png"
  },
  {
    name: "XANH CHUỐI",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/XANH CHUOI.png"
  },
  {
    name: "XANH GHI",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/XANH GHI.png"
  },
  {
    name: "XANH LÁ",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/XANH LA.png"
  },
  {
    name: "XANH NHẠT",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/XANH nhat.png"
  },
  {
    name: "XANH THẪM",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/XANH THAM.png"
  },
  {
    name: "XANH TÍM THAN",
    value: "D:/SXCT/1.CG VMIX/BÓNG RỔ/MAU AO/XANH TIM THAN.png"
  }
]
export const textColor = [
  {
    name: "MÀU TRẮNG",
    value: "#FFFFFF"
  },
  {
    name: "MÀU ĐEN",
    value: "#000000"
  },
  {
    name: "MÀU XANH",
    value: "#0000FF"
  }
]
