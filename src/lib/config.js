export const configThumbVideoEvent = {
  upload: [{ width: 720, height: 405 }],
  name: { mobile: "720X405", website: "720X405", smart_tv: "" }
}

export const configThumbTopic = {
  upload: [
    { width: 720, height: 240 },
    { width: 1920, height: 640 }
  ],
  name: { mobile: "390X130", website: "1920X640", smart_tv: "1920X640" }
}

export const configThumbSlide = {
  upload: [
    { width: 160, height: 90 },
    { width: 720, height: 405 },
    { width: 1280, height: 720 }
  ],
  name: { mobile: "720X405", website: "720X405", smart_tv: "", small: "160x90" }
}
export const configThumbVertical = {
  upload: [{ width: 400, height: 600 }],
  name: { mobile: "400X600", website: "400X600", smart_tv: "" }
}
