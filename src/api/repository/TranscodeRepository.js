import Client from "../client/Client"
const resource = "api"

function getListTranscode() {
  return Client.get(`${resource}/od_encoding_jobs`, {})
}

const api = {
  getListTranscode
}
export default api
