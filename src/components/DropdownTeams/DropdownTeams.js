import { useState, useEffect, forwardRef, memo, useRef } from "react"
import { apiTeams } from "api"
import { Select } from "antd"

const Component = forwardRef(({ onChange, value }, ref) => {
  const [rows, setRows] = useState([])
  const [loading, setLoading] = useState(false)
  const isMonter = useRef(true)

  const fetch = async () => {
    setLoading(true)
    let _rows = []
    try {
      const { results } = await apiTeams.getListTeams({ page_size: 200 })
      _rows = results.map((i) => {
        return {
          label: i.name,
          value: i.id
        }
      })
    } catch (e) {
      console.log(e)
    } finally {
      if (isMonter.current) {
        setRows(_rows)
        setLoading(false)
      }
    }
  }
  useEffect(() => {
    fetch()
    return () => {
      isMonter.current = false
    }
  }, [])

  return (
    <Select
      ref={ref}
      showSearch
      loading={loading}
      style={{ width: "100%" }}
      dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
      placeholder="Teams..."
      allowClear
      options={rows}
      value={value}
      onChange={onChange}
      filterOption={(input, option) => {
        return option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }}
    />
  )
})

export default memo(Component)
