import { lazy } from "react"

const Transcode = lazy(() => import("pages/Transcode"))

//____ListPage
function pageList(__role) {
  return [
    {
      Element: Transcode,
      path: "transcode",
      title: "Transcode",
      code: "CHANNEL_CONTROLLER"
    }
  ]
}

export default pageList
