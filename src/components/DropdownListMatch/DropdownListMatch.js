import { useState, useEffect, forwardRef, memo, useRef } from "react"
import { apiMatch } from "api"
import { Select, Tag, Button } from "antd"
import { CloseCircleOutlined } from "@ant-design/icons"
const { Option } = Select

const Component = forwardRef(({ onChange, value }, ref) => {
  const [rows, setRows] = useState([])
  const [loading, setLoading] = useState(false)
  const isMonter = useRef(true)

  const fetch = async () => {
    setLoading(true)
    let _rows = []
    try {
      const { results } = await apiMatch.getListMatchs({ page_size: 200 })
      console.log("resultsresults", results)
      _rows = results
    } catch (e) {
      console.log(e)
    } finally {
      if (isMonter.current) {
        setRows(_rows)
        setLoading(false)
      }
    }
  }

  useEffect(() => {
    fetch()
    return () => {
      isMonter.current = false
    }
  }, [])

  return (
    <Select
      ref={ref}
      showSearch
      loading={loading}
      style={{ width: "100%" }}
      dropdownStyle={{ maxHeight: 400, overflow: "auto" }}
      placeholder="Match..."
      allowClear
      onChange={onChange}
      filterOption={(input, option) => {
        return option.label.toLowerCase().indexOf(input.toLowerCase()) >= 0
      }}
    >
      {rows?.map(({ id, name, isManual }) => {
        return (
          <Option key={id} value={id} name={name}>
            <div className="flex justify-between gap-2">
              <span>{name}</span>
              <Tag color={isManual === false ? "blue" : "green"}>
                {isManual === false ? "AUTO" : "MANUAL"}
              </Tag>
            </div>
          </Option>
        )
      })}
    </Select>
  )
})

export default memo(Component)
