import React from "react"
import { Dropdown } from "antd"
import { useNavigate, Link } from "react-router-dom"
import { UserOutlined, LoginOutlined } from "@ant-design/icons"
import { useStore } from "components/ui"

const MyMenu = () => {
  const { logOut } = useStore()
  const navigate = useNavigate()

  function handleLogout() {
    logOut()
    navigate("/login")
  }

  return (
    <div className="p-4 shadow_antd space-y-4 bg-white rounded">
      <Link to="/profile" className="cursor-pointer text-slate-900 hover:text-primary">
        <UserOutlined /> &nbsp;Account Infomation
      </Link>

      <div onClick={handleLogout} className="cursor-pointer hover:text-primary">
        <LoginOutlined />
        &nbsp; Log Out
      </div>
    </div>
  )
}

const Header = ({ user }) => {
  return (
    <div className="flex">
      <Dropdown overlay={<MyMenu />} placement="bottomLeft">
        <div className="cursor-pointer flex jutify-center items-center">
          <div className="flex items-center justify-center hover-sb mr-2"></div>
          <span className="user-name text-white">{user?.name}</span>
          <div className="h-6 w-6 hidden"></div>
        </div>
      </Dropdown>
    </div>
  )
}

export default Header
